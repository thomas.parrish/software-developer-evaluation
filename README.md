# Introduction 
Design and implement a RESTful WebAPI for managing webhook subscriptions using .Net Core 7, Entity Framework, and Angular 16. The API should consist of a single controller with CRUD endpoints for a webhook subscription, along with an accompanying front end with list, create, edit, and delete views. No authentication or authorization is necessary for the purposes of this evaluation. You are free to choose a system architecture of your preference, but should adhere to best coding practices (i.e., separation of concerns and IoC.) A few notes on expectations:

- A pull request should be submitted within 48 hours of forking the repository.
- The webhook callbacks are expected to use HMAC signatures for authentication, so some consideration should be given to how to manage a shared secret.
- The API should generate an OpenAPI Scheme (swagger.json)
- The data does not need to be persistent, but should utilize Entity Framework (i.e., EF Core In Memory database is fine.)
- Any front end framework may be used (ngx-bootstrap, primeng, material, etc. ) but Angular Material would be preferred.
- For the GET (All) endpoint, paging is optional.
- Testing is expected, but integration tests can be leveraged in place of unit tests based on personal preference.

__An application that fails to meet all requirements will still be evaluated, but *only if the project compiles and all tests are passing.*__